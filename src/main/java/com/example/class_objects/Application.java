package com.example.class_objects;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@SpringBootApplication
public class Application {

    public static void main(String[] args) {
//        SpringApplication.run(Application.class, args);
        System.out.println("hello java classes and objects");
        int zahl1 = 1 ;
        Integer zahl2 = 2;
        // we build a house of the family Smith
        House house = new House();
        // we set now the fields
        house.setFamilyName("Smith");
        house.setHouseNumber(1291);
        house.setStreet("Mainstreet");
        house.setNumberOfRooms(6);
        house.setPrice(1000000); // $

        House myHouse = new House();
        myHouse.setFamilyName("Cavuoti");

        // read the values and print it
        String familyName = house.getFamilyName();
        System.out.println(familyName);

        System.out.println(house.getNumberOfRooms());

        // toString Methode
        System.out.println(house.toString());

    }

}
