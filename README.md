# Introduction to the OO-Programming with java

##Content
- explaining the concepts of classes and object 
- basic example of a class 


# Clone the project
```bash
git clone https://gitlab.com/lcavuoti/m319-klassen-objekte.git
```

## contribute to the project
Teachers of the School BBW can
- contribute to the project by adding a pull request.
- contribute by writing a message to the author.
